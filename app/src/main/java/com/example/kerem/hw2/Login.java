package com.example.kerem.hw2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class Login extends Activity {
    Button register, login;
    EditText mail, password;
    String mailtxt, passtxt;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = preferences.edit();

        if (preferences.getBoolean("login", false)) {
            Intent i = new Intent(getApplicationContext(), Homepage.class);
            startActivity(i);
            finish();
        }


        mail = (EditText) findViewById(R.id.editText1);
        password = (EditText) findViewById(R.id.editText2);

        register = (Button) findViewById(R.id.button1);
        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Register.class);
                startActivity(i);
            }
        });

        login = (Button) findViewById(R.id.button2);
        login.setOnClickListener(new View.OnClickListener() {//giriş butonu tıklandığı zaman

            @Override
            public void onClick(View v) {
                mailtxt = mail.getText().toString();
                passtxt = password.getText().toString();

                if (mailtxt.matches("")) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(Login.this);
                    alertDialog.setTitle("Warning!");
                    alertDialog.setMessage("Please fill out the form!");
                    alertDialog.setPositiveButton("Okey", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                } else {

                    String email = preferences.getString("email", "");
                    String pass = preferences.getString("sifre", "");

                    if (email.matches(mailtxt) && pass.matches(passtxt)) {


                        editor.putBoolean("login", true);
                        Intent i = new Intent(getApplicationContext(), Homepage.class);
                        startActivity(i);
                        finish();
                    } else {//şifre ve mail uyuşmuyorsa hata bastırılır
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Login.this);
                        alertDialog.setTitle("Error!");
                        alertDialog.setMessage("Name and Password didn't match!");
                        alertDialog.setPositiveButton("Okey", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertDialog.show();
                    }
                }
            }
        });
    }

}
