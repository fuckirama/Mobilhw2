package com.example.kerem.hw2;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Register extends Activity {

    Button register;
    EditText namsur, email, password, age;
    String namsurtxt, emailtxt, passtxt;
    int agenum;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        register = (Button) findViewById(R.id.button1);
        namsur = (EditText) findViewById(R.id.editText1);
        email = (EditText) findViewById(R.id.editText2);
        age = (EditText) findViewById(R.id.editText4);
        password = (EditText) findViewById(R.id.editText3);


        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                namsurtxt = namsur.getText().toString();
                emailtxt = email.getText().toString();
                passtxt = password.getText().toString();
              //  agenum = Integer.parseInt(age);
                if (namsurtxt.matches("") || emailtxt.matches("")) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(Register.this);
                    alertDialog.setTitle("Warning!");
                    alertDialog.setMessage("Please fill out the form!");
                    alertDialog.setPositiveButton("Okey!", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                } else {

                    preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    editor = preferences.edit();


                    editor.putString("namsur", namsurtxt);
                    editor.putString("email", emailtxt);
                    editor.putString("pass", passtxt);
                    editor.putInt("age", agenum);
                    editor.putBoolean("login", true);


                    editor.commit();
                    Intent i = new Intent(getApplicationContext(), Homepage.class);
                    startActivity(i);
                    finish();
                }

            }
        });
    }
}
